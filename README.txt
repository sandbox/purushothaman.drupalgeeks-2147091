Commerce Coupon Redeem
======================

Description
-----------

Commerce Coupon Redeem module provides a form 
for redeeming Commerce Coupon on cart page.

Other Administer settings are, 

1. Can enable/disable the option of displaying the 
Commerce Coupon Redemption form on cart page.

2. Can enable/disable the option of displaying the 
table of redeemed coupon code and granted amount 
with removing action on cart page.

Sponsored by DrupalGeeks.org (http://www.drupalgeeks.org/).

Dependencies
------------

Drupal Commerce and all of its dependencies
Commerce Coupon


Installation
------------

Normal installation just like other modules. Install module on site/all
/contrib/module/ folder and enable it.
